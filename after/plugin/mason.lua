require('mason').setup()
require('mason-lspconfig').setup()
local common = require('lib')
-- local common = require('~/.config/nvim/lua/lib')

require('mason-nvim-dap').setup({
	handlers = {
		function(config)
			if config.configurations then
				for _, value in ipairs(config.configurations) do
					local func = Type_tbl[value.type]
					if (func) then
						func(value)
					end
				end
			end
			require('mason-nvim-dap').default_setup(config)
		end
	}
})

local lsp = require('lsp-zero')
lsp.extend_cmp()
