local lualine = require('lualine')

lualine.setup {
	options = {
		icons_enabled = true,
		-- theme = 'ayu_dark',
		-- theme = 'gruber-darker',
		theme = 'auto',
		sections = {
			lualine_a = { 'mode', 'paste' },
			lualine_b = { 'branch', 'readonly', 'filename', 'modified' }
		}
	}
}
