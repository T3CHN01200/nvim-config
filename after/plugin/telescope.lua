local builtin = require('telescope.builtin')

vim.keymap.set("n", "<leader>ff", builtin.find_files)
vim.keymap.set("n", "<leader>gf", builtin.git_files)
vim.keymap.set("n", "<leader>fg", builtin.live_grep)
vim.keymap.set("n", "<leader>fb", builtin.buffers)
vim.keymap.set("n", "<leader>fh", builtin.help_tags)

vim.keymap.set("n", "<leader>фф", builtin.find_files)
vim.keymap.set("n", "<leader>гф", builtin.git_files)
vim.keymap.set("n", "<leader>фг", builtin.live_grep)
vim.keymap.set("n", "<leader>фб", builtin.buffers)
vim.keymap.set("n", "<leader>фх", builtin.help_tags)
