local lspZero = require('lsp-zero').preset({})
local lsp = require('lspconfig')
local cmp = require'cmp'
local cmplsp = require'cmp_nvim_lsp'
local luasnip = require'luasnip'
local osExt = require('omnisharp_extended')

local capabilities = cmplsp.default_capabilities()

lspZero.on_attach(function(_, bufnr)
	lspZero.default_keymaps({buffer = bufnr})
	vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end)
	-- vim.keymap.set("n", "<leader>gd", telescope.lsp_definitions)
	vim.keymap.set("n", "gi", function() vim.lsp.buf.implementation() end)
	vim.keymap.set("n", "gh", function() vim.lsp.buf.hover() end)
	vim.keymap.set("n", "gH", function() vim.lsp.buf.code_action() end)
	vim.keymap.set("n", "gr", function() vim.lsp.buf.references() end)
	vim.keymap.set("n", "F2", function() vim.lsp.buf.rename() end)
	vim.keymap.set("n", "гд", function() vim.lsp.buf.definition() end)
	-- vim.keymap.set("n", "<leader>gd", telescope.lsp_definitions)
	vim.keymap.set("n", "ги", function() vim.lsp.buf.implementation() end)
	vim.keymap.set("n", "гх", function() vim.lsp.buf.hover() end)
	vim.keymap.set("n", "гХ", function() vim.lsp.buf.code_action() end)
	vim.keymap.set("n", "гр", function() vim.lsp.buf.references() end)
	-- client.server_capabilities.semanticTokensProvider = nil
end)

-- (Optional) Configure lua language server for neovim
lsp.lua_ls.setup(lspZero.nvim_lua_ls())

lsp.omnisharp.setup {
	filetypes = {
		"cs",
		"vb",
		"cshtml",
		"razor",
		"aspnetcorerazor"
	},
	settings = {
		FormattingOptions = {
			OrganizeImports = true,
		}
	},
	RoslynExtensionsOptions = {
		EnableImportCompletion = true,
		EnableDecompilationSupport = true,
	},
	handlers = {
		["textDocument/definition"] = osExt.definition_handler,
		["textDocument/typeDefinition"] = osExt.type_definition_handler,
		["textDocument/references"] = osExt.references_handler,
		["textDocument/implementation"] = osExt.implementation_handler,
	}
}

lsp.emmet_ls.setup {
	filetypes = {
		"astro",
		"css",
		"eruby",
		"html",
		"htmldjango",
		"javascript",
		"javascriptreact",
		"less",
		"pug",
		"sass",
		"scss",
		"svelte",
		"typescriptreact",
		"vue",
		"cshtml",
		"razor",
		"aspnetcorerazor"
	}
}

lsp.ols.setup({
	cmd = { "ols", "-stdin" },
	filetypes = { "odin" },
	capabilities = capabilities,
})

lsp.ts_ls.setup({
	settings = {
		implicitProjectConfiguration = {
			checkJs = true
		}
	}
})

lspZero.setup()

cmp.setup {
	mapping = cmp.mapping.preset.insert({
		['<C-u>'] = cmp.mapping.scroll_docs(-4), -- Up
		['<C-d>'] = cmp.mapping.scroll_docs(4), -- Down
		['<C-Space>'] = cmp.mapping.complete(),
		['<CR>'] = cmp.mapping.confirm {
			behavior = cmp.ConfirmBehavior.Insert,
			select = true,
		},
		['<Tab>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			else
				fallback()
			end
		end, { 'i', 's' }),
		['<S-Tab>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			else
				fallback()
			end
		end, { 'i', 's' }),
	}),
	sources = {
		{ name = 'nvim_lsp' },
		{ name = 'luasnip' },
		{ name = 'nvim_lssp_signature_help' },
		{ name = 'path' },
		{ name = 'buffer' },
	},
	snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end
	}
}
