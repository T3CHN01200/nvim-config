vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'
	use ({
		'nvim-telescope/telescope.nvim',
		requires = {{ 'nvim-lua/plenary.nvim'}}
	})
	use ({
		'folke/tokyonight.nvim',
		-- config = function()
		-- 	vim.cmd('colorscheme tokyonight-night')
		-- end
	})
	use ({
		'folke/trouble.nvim'
	})
	use ({
		'AlessandroYorba/Alduin',
		-- config = function()
		-- 	vim.o.background="dark"
		-- 	vim.g.alduin_Shout_Dragon_Aspect=true
		-- 	vim.cmd('colorscheme alduin')
		-- end
	})
	use ({
		'VictorSohier/nite',
		-- config = function()
		-- vim.cmd.colorscheme("nite")
		-- end
	})
	use ({
		'blazkowolf/gruber-darker.nvim',
		config = function()
			vim.cmd('colorscheme gruber-darker')
			-- vim.cmd('colorscheme smyck')
		end
	})
	use ({
		'sainnhe/gruvbox-material',
		-- config = function()
		-- 	vim.o.background="dark"
		-- 	vim.cmd('colorscheme gruvbox-material')
		-- end
	})
	use ({
	 	'nvim-treesitter/nvim-treesitter-context',
		requires = {
			{
				'nvim-treesitter/nvim-treesitter',
				{
					run = 'TSUpdate'
				}
			}
		},
	})
	use('tpope/vim-fugitive')
	use('tpope/vim-commentary')
	use('airblade/vim-gitgutter')
	use {
		'VonHeikemen/lsp-zero.nvim',
		branch = 'v2.x',
		requires = {
			{
				'neovim/nvim-lspconfig',
				opts = {
					autoformat = false,
				}
			},
			{
				'williamboman/mason.nvim',
				run = function()
					pcall(vim.cmd, 'MasonUpdate')
				end,
			},
			{'williamboman/mason-lspconfig.nvim'},
			{'hrsh7th/nvim-cmp'},
			{'hrsh7th/cmp-nvim-lsp'},
			{'hrsh7th/cmp-nvim-lsp-signature-help'},
			{'L3MON4D3/LuaSnip'},
		}
	}
	use {
		'rcarriga/nvim-dap-ui',
		requires = {
			'mfussenegger/nvim-dap',
			'jay-babu/mason-nvim-dap.nvim'
		}
	}
	use {'nvim-telescope/telescope-dap.nvim'}
	use {'fidian/hexmode'}
	use {'christoomey/vim-tmux-navigator'}
	use {'nvim-lualine/lualine.nvim'}
	use {'shime/vim-livedown'}
	use {'tpope/vim-pathogen'}
	use {
		"windwp/nvim-autopairs",
	    config = function() require("nvim-autopairs").setup {} end
	}
	-- use {'ziglang/zig.vim'}
	use {'mbbill/undotree'}
	use {'adamclerk/vim-razor'}
	use {'nvim-treesitter/playground'}
	use {'nvim-neotest/nvim-nio'}
	use {'Hoffs/omnisharp-extended-lsp.nvim'}
end)
