local dapUtils = require('dap.utils');

local args = "";
local cwd = vim.fn.getcwd()
local program = vim.fn.getcwd()

function GetProgram()
	local tProgram = vim.fn.input("Path to executable: ", program, "file")
	if tProgram ~= "" then
		program = tProgram
	end
	return program
end

function GetArgs()
	local tArgs = vim.fn.input("CLI Args: ", args, "file")
	if tArgs ~= "" then
		args = tArgs
	end
	return dapUtils.splitstr(args)
end

function GetCwd()
	local tCwd = vim.fn.input("Working Directory: ", cwd, "file")
	if tCwd ~= "" then
		cwd = tCwd
	end
	return cwd
end

local function setConfig(config)
	config.name = "Launch File"
	config.request = "launch"
	config.stopOnEntry = false
	config.program = GetProgram
	config.args = GetArgs
	config.cwd = GetCwd
end

Type_tbl = {
	["codelldb"] = setConfig,
	["coreclr"] = setConfig,
}

function Dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. Dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end
