if exists("did_load_filetypes")
	finish
endif

augroup filetypedetect
	au! BufRead,BufNewFile *.md.txt setfiletype markdown
	au! BufRead,BufNewFile *.razor setfiletype html
	au! BufRead,BufNewFile *.{vert,tesc,tese,geom,frag,comp} setfiletype glsl
augroup END
